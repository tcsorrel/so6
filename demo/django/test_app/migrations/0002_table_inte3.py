# Generated by Django 3.2.9 on 2021-12-09 10:41

from django.db import migrations
from django.db import models


class Migration(migrations.Migration):

    dependencies = [
        ("test_app", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="table",
            name="inte3",
            field=models.IntegerField(default=0),
        ),
    ]
