from so6.tasks import TaskBase


class So6Task(TaskBase):
    @staticmethod
    def ready():
        return True

    @staticmethod
    def remains():
        return 5
