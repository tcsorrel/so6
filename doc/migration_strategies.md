---
marp: true
---

![bg left:40% 80%](migration_with_django.svg)
# So6 migration with django schema migration  
- Add dependency D
- (makemigrations --merge) if required
- Schedule So6 task
---

![bg left:40% 70%](migration_with_sql.svg)
# So6 migration with SQL schema migration
Schema migration is done by So6 Task itself   

---

![bg left:40% 50%](migration_index.svg)
# So6 migration for index creation  
So6 Task for index creation must be scheduled only once when server isn't heavily loaded
