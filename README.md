# So6

Ease background data migration in a django application.

## Architectiture
So6 defines one main task which is the heart bit of all the background
actions : check status and run a data migration task.

Checks to do on data migration tasks:
  1. is the dependance in django tree is full field ? => update task.activated
  2. is there is something left to do ? update task.to_be_done

Task application:
  Randomly choose a So6 app task and execute it.

So6 will provide a REST entry point to know what remains to be done.

## task
### location
 - prj/app/so6/task1.py
 - prj/app/so6/task2.py
### content
So6 task file must contain a class called So6Task inherting from so6.tasks.TaskBase
TaskBase is the scheleton of background data migration
Each task defines:
  1. A reference in django app migration tree to ensure the database schema
  context in which the data migration task is run 
  2. An atomic and small treatment of data migration
  3. A request to mesure what remains to be done

## Settings:
### task settings:
- limit
