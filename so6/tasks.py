"""
defines TaskBase interface for background app tasks
and the main processus
"""
from django.db import connection
from django.db import transaction
from django.db.migrations.recorder import MigrationRecorder

from .models import Task


class TaskBase:
    """
    1. A reference in django app migration tree to ensure the database schema
    context in which the data migration task is run
    2. An atomic and small treatment of data migration
    3. A request to mesure what remains to be done
    """

    MIGRATION_REQUIRED = None
    QUERY_REQUIRED = None
    REMAINS_QUERY = None
    NEEDED_COLUMN = None
    NEEDED_IN_TABLE = None
    CREATE_NEEDED_COLUMN = None
    NEEDED_CONSTRAINT = None
    CREATE_NEEDED_CONSTRAINT = None
    MAIN_QUERY = None

    def __init__(self):
        app, _, name = self.__module__.split(".")
        self.store, _ = Task.objects.get_or_create(
            app=app,
            name=name,
            defaults={"to_be_done": 0, "activated": False, "limit": 1},
        )
        self.store.activated = self.ready()
        self.store.save()

    def update_status(self):
        """
        Updates remaining count. It may be long
        """
        self.store.to_be_done = self.remains()
        self.store.save()

    def ready(self):
        """
        returns if database status is ready for data migration or not
        """
        if self.MIGRATION_REQUIRED:
            return (
                MigrationRecorder(connection)
                .migration_qs.filter(app=self.store.app, name=self.MIGRATION_REQUIRED)
                .exists()
            )
        if self.QUERY_REQUIRED:
            with connection.cursor() as cursor:
                cursor.execute(self.QUERY_REQUIRED)
                return cursor.fetchone()
        raise Exception("MIGRATION_REQUIRED or QUERY_REQUIRED must be set")

    def execute(self):
        """
        atomic data migration treatment to perform
        """
        if not self.ready():
            return
        self._run(limit=self.store.limit)

    def _create_constraint_if_required(self, cursor):
        cursor.execute(
            """
            SELECT 1
            FROM information_schema.constraint_column_usage
            WHERE constraint_name = %s
            """,
            (self.NEEDED_CONSTRAINT),
        )
        if not cursor.fetchone():
            cursor.execute(self.CREATE_NEEDED_CONSTRAINT)

    def _create_column_if_required(self, cursor):
        cursor.execute(
            """
            SELECT 1
            FROM information_schema.columns
            WHERE table_name = %s and column_name = %s
            """,
            (self.NEEDED_IN_TABLE, self.NEEDED_COLUMN),
        )
        if not cursor.fetchone():
            cursor.execute(self.CREATE_NEEDED_COLUMN)

    def migrate(self, *args):
        """
        Called by migration to migrate all remanings if ever missed during background execute
        """
        if not self.ready():
            raise Exception(
                f"Migration {self.store.name} of {self.store.app} isn't ready ?"
            )
        self._run()
        # self.store.finished = True
        # self.store.save()

    def _run(self, **kwargs):
        with connection.cursor() as cursor:
            with transaction.atomic():
                cursor.execute("SET session_replication_role TO 'replica'")
                if self.NEEDED_COLUMN:
                    self._create_column_if_required(cursor)
                if self.NEEDED_CONSTRAINT:
                    self._create_constraint_if_required(cursor)
                cursor.execute(self.MAIN_QUERY, [kwargs.get("limit") or None])
                counter = cursor.rowcount
                cursor.execute("SET session_replication_role TO 'origin'")
        return counter

    def remains(self):
        """
        returns the number of entry that remains to be done
        """
        if self.REMAINS_QUERY:
            with connection.cursor() as cursor:
                cursor.execute(self.REMAINS_QUERY)
                return cursor.fetch_row()[0]
        raise Exception("REMAINS_QUERY must be set")


class IndexTask(TaskBase):
    """
    Index creation dedicated task
    Index should be created only once when the server has a low load
    """

    INDEX_NAME = None
    INDEXED_FIELD = None
    NEEDED_IN_SCHEMA = None

    def ready(self):
        readiness = True
        with connection.cursor() as cursor:
            for field in (f.strip() for f in self.INDEXED_FIELD.split(",")):
                cursor.execute(
                    """
                    SELECT 1
                    FROM information_schema.columns
                    WHERE table_name = %s and column_name = %s
                    """,
                    (self.NEEDED_IN_TABLE, field),
                )
                readiness &= bool(cursor.fetchone())
        return readiness

    def _run(self, **kwargs):
        if not self.INDEX_NAME:
            return
        with connection.cursor() as cursor:
            with transaction.atomic():
                cursor.execute("SET session_replication_role TO 'replica'")
                cursor.execute(
                    """SELECT 1 FROM pg_indexes WHERE indexname = %s and tablename = %s""",
                    (self.INDEX_NAME, self.NEEDED_IN_TABLE),
                )
                if not cursor.fetchone():
                    if kwargs.get("concurrently", False):
                        cursor.execute(
                            "CREATE INDEX CONCURRENTLY IF NOT EXISTS {} ON {}.{} ({});".format(
                                self.INDEX_NAME,
                                self.NEEDED_IN_SCHEMA,
                                self.NEEDED_IN_TABLE,
                                self.INDEXED_FIELD,
                            )
                        )
                    else:
                        cursor.execute(
                            "CREATE INDEX IF NOT EXISTS {} ON {}.{} ({});".format(
                                self.INDEX_NAME,
                                self.NEEDED_IN_SCHEMA,
                                self.NEEDED_IN_TABLE,
                                self.INDEXED_FIELD,
                            )
                        )
                cursor.execute("SET session_replication_role TO 'origin'")
