from random import choice

from django.core.management.base import BaseCommand

from so6.main import search_tasks


class Command(BaseCommand):
    def handle(self, *args, **options):
        tasks, _ = search_tasks()
        counter = choice(tasks.values()).execute()
        self.stdout.write(f"{counter}")
        return counter
