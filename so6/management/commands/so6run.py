from django.core.management.base import BaseCommand

from so6.main import search_tasks


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_arguments("app")
        parser.add_arguments("name")

    def handle(self, *args, **options):
        tasks, indexes = search_tasks()
        tasks.update(indexes)  # here we want to run a task whatever is its type
        tasks[f"{options['app']}.so6.{options['name']}"].execute()
