from django.core.management.base import BaseCommand

from so6.main import search_tasks


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        tasks, indexes = search_tasks()
        for task in tasks.values():
            self.stdout.write(
                f"A task is called {task.store.name} in app {task.store.app}"
            )
        for task in indexes.values():
            self.stdout.write(
                f"An indexe task is called {task.store.name} in app {task.store.app}"
            )
