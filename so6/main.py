"""
defines the main processus
"""
from importlib import import_module
import logging
import os
import pkgutil

from django.apps import apps

from .tasks import IndexTask


def search_tasks():
    """
    find so6 tasks and create tasks in database accordinly
    """
    tasks = {}
    indexes = {}
    for app_config in apps.get_app_configs():
        so6_dir = os.path.join(app_config.path, "so6")
        if not os.path.exists(so6_dir):
            continue
        for _, name, is_pkg in pkgutil.iter_modules([so6_dir]):
            if is_pkg:
                continue
            task_key = f"{app_config.name}.so6.{name}"
            try:
                logging.info(task_key)
                task = import_module(task_key).So6Task()
            except:
                logging.exception("%s not defined as a So6Task", task_key)
                continue
            if isinstance(task, IndexTask):
                indexes[task_key] = task
            else:
                tasks[task_key] = task
    return (tasks, indexes)
