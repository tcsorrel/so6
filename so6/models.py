"""
ORM model definition for So6 task monitoring
"""
from django.db import models


class Task(models.Model):
    """
    So6 task monitoring definition
    """

    id = models.BigAutoField(
        "ID", primary_key=True
    )  # freeze migration even if django < 3.2
    app = models.CharField(max_length=64)
    name = models.CharField(max_length=64)
    to_be_done = models.IntegerField()
    activated = models.BooleanField()
    limit = models.IntegerField()
    # finished = models.BooleanField()

    def __str__(self):
        return "Task " + self.name + " from app " + self.app
