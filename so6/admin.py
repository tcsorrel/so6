from django.contrib import admin

from .models import Task


class TaskAdmin(admin.ModelAdmin):
    readonly_fields = ("app", "name", "to_be_done", "activated")


# Register your models here.
admin.site.register(Task, TaskAdmin)
